[dynamodb://<name>]
* Inputs timestamped rows from a DynamoDB table
table_name = <value>
pk_fieldname = <value>
pk = <value>
timestamp_fieldname = <value>
aws_key = <value>
aws_secret = <value>
aws_region = <value>
last_timestamp = <value>
