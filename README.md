# Splunk app to pull rows from a DynamoDB table
This app provides a modular input that pulls timestamped rows from an AWS-hosted DynamoDB table into Splunk. The content of rows is formatted as JSON for easy extraction of Splunk fields.

Your DynamoDB table should have a partition key column and a sort key column containing the UNIX timestamp (seconds or milliseconds since epoch) when it was inserted.

## Requirements
Splunk 6.0 or later.

This app can be installed in any search head, indexer, or light or heavy forwarder. The universal forwarder is currently not compatible because it lacks the necessary Python libraries.

## Setting up
This app uses your system Python install instead of Splunk's built in one, allowing PIP packages to be used. Setup is as follows:

- Install the Splunk app through the GUI or by extracting it to your apps directory and restarting Splunk.
- Configure the modular input through the Splunk GUI:
    - Set `table_name` to the name of your DynamoDB table.
	- Set `pk_field` to the name of your DynamoDB table's partition key.
	- Set `pk` to the desired value of the key.
	- Set `timestamp_fieldname` to the name of the column containing your timestamps.
	- Set `aws_key` to the API key of an AWS user capable of reading your table.
	- Set `aws_secret` to the API secret of the same user.
	- Set `aws_region` to the appropriate AWS region for your DynamoDB setup.
	- To specify a starting point for the initial fetch, set `last_timestamp` to the appropriate UNIX timestamp. Otherwise, leave it blank and the script will fetch all rows of the table. The script will automatically update this value as it fetches rows.
	- Under "More settings", set `Interval` to your desired fetch frequency so that the script will re-run itself periodically. Optionally set sourcetype, index, etc.

## Events returned
The modular input passes your database rows to Splunk in the form of JSON dictionaries. The content of the timestamp column is automatically passed as the event timestamp.

## Acknowledgements
The following libraries are bundled with this script.

- [Splunk SDK](https://pypi.python.org/pypi/splunk-sdk)
