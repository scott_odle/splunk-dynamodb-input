#!/usr/bin/env bash
PACKAGE_NAME="splunk-dynamodb-input"
PIP=$(command -v pip || command -v pip2 || command -v pip3)

echo "Removing existing builds..."
rm -rf "$PACKAGE_NAME"
rm -rf "$PACKAGE_NAME.tgz"

echo "Creating app directory..."
mkdir $PACKAGE_NAME
cp -r bin default README static README.md $PACKAGE_NAME

echo "Installing Python dependencies..."
mkdir "$PACKAGE_NAME/lib"
$PIP install -r README/requirements.txt --target="$PACKAGE_NAME/lib"

echo "Cleaning up Python binaries..."
find $PACKAGE_NAME -type f -name "*.pyc" -delete
find $PACKAGE_NAME -type f -name "*.pyo" -delete

echo "Fixing permissions..."
find $PACKAGE_NAME -type f -exec chmod 644 -- {} +

echo "Creating tarball..."
tar czf $PACKAGE_NAME.tgz $PACKAGE_NAME
