import boto3
import json
import sys
from os import path
import datetime
from decimal import Decimal
from boto3.dynamodb.conditions import Key

sys.path.insert(0, path.join(path.dirname(__file__), "..", "lib"))
from splunklib.modularinput import Script, Scheme, Argument, Event


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Decimal):
            return str(o)
        return super(DecimalEncoder, self).default(o)


def get_dynamodb_client(region, access_key, access_secret):
    return boto3.resource('dynamodb', region_name=region,
                          aws_access_key_id=access_key,
                          aws_secret_access_key=access_secret)


class DynamoDBModularInput(Script):
    def get_scheme(self):
        scheme = Scheme('AWS DynamoDB Modular Input')
        scheme.description = ('Pull data from a DynamoDB table. Your table'
                              ' must have a field tracking the insert date of'
                              ' each row in order to keep from fetching'
                              ' duplicate rows.')

        table_name = Argument('table_name')
        table_name.description = ('Name of the table to fetch data from. '
                                  'Must have a timestamp field for each row.')
        table_name.data_type = Argument.data_type_string
        table_name.required_on_create = True
        scheme.add_argument(table_name)

        pk_field = Argument('pk_fieldname')
        pk_field.description = ('Name of the partition key field for this '
                                'table')
        pk_field.data_type = Argument.data_type_string
        pk_field.required_on_create = True
        scheme.add_argument(pk_field)

        pk = Argument('pk')
        pk.description = 'Get values with this partition key'
        pk.data_type = Argument.data_type_string
        pk.required_on_create = True
        scheme.add_argument(pk)

        timestamp_field = Argument('timestamp_fieldname')
        timestamp_field.description = ('Name of the timestamp sort key field'
                                       'for this table')
        timestamp_field.data_type = Argument.data_type_string
        timestamp_field.required_on_create = True
        scheme.add_argument(timestamp_field)

        aws_region = Argument('aws_region')
        aws_region.description = 'From your AWS account info.'
        aws_region.data_type = Argument.data_type_string
        aws_region.required_on_create = True
        scheme.add_argument(aws_region)

        aws_access_key = Argument('aws_key')
        aws_access_key.description = 'From your AWS users page.'
        aws_access_key.data_type = Argument.data_type_string
        aws_access_key.required_on_create = True
        scheme.add_argument(aws_access_key)

        aws_access_secret = Argument('aws_secret')
        aws_access_secret.description = 'From your AWS users page.'
        aws_access_secret.data_type = Argument.data_type_string
        aws_access_secret.required_on_create = True
        scheme.add_argument(aws_access_secret)

        last_timestamp = Argument('last_timestamp')
        last_timestamp.description = ('Timestamp of the last row fetched from'
                                      ' DynamoDB. If you this blank, the'
                                      ' input will fetch all rows available'
                                      ' and update it to the latest time'
                                      ' for you.')
        last_timestamp.data_type = Argument.data_type_number
        scheme.add_argument(last_timestamp)

        return scheme

    def get_rows_since(self, dynamo_client, table_name, pk_field,
                       pk, timestamp_field, last_timestamp, ew):
        table = dynamo_client.Table(table_name)
        pk_filter = Key(pk_field).eq(pk)
        ts_filter = Key(timestamp_field).gt(str(last_timestamp))
        filter_exp = pk_filter & ts_filter
        ew.log('INFO', 'filter: ' + pk_field + ' = ' + pk)
        ew.log('INFO', 'filter: ' + timestamp_field + ' > ' + str(last_timestamp))
        results = table.query(KeyConditionExpression=filter_exp)
        return results['Items']

    def update_modular_input(self, input_name, input_item, new_timestamp, ew):
        serv_inputs = self.service.inputs
        for serv_input_item in serv_inputs:
            if "{}://{}".format(serv_input_item.kind, serv_input_item.name) == input_name:
                serv_input_item.update(last_timestamp=new_timestamp)
                break

    def stream_events(self, inputs, ew):
        for input_name in inputs.inputs:
            input_item = inputs.inputs[input_name]
            aws_key = input_item['aws_key']
            aws_secret = input_item['aws_secret']
            aws_region = input_item['aws_region']
            aws_client = get_dynamodb_client(aws_region, aws_key,
                                                  aws_secret)
            pk_field = input_item['pk_fieldname']
            pk = input_item['pk']
            table_name = input_item['table_name']
            timestamp_field = input_item['timestamp_fieldname']
            last_timestamp = input_item['last_timestamp']
            if last_timestamp is None:
                last_timestamp = 0
            last_timestamp = int(last_timestamp)

            rows = self.get_rows_since(aws_client, table_name, pk_field, pk,
                                       timestamp_field, last_timestamp, ew)

            for row in rows:
                evt_time = 0
                try:
                    epoch = float(row[timestamp_field])
                    evt_time = datetime.datetime.utcfromtimestamp(epoch)
                except ValueError:
                    epoch = float(row[timestamp_field]) / 1000
                    evt_time = datetime.datetime.utcfromtimestamp(epoch)
                evt_timestring = evt_time.strftime('%Y-%m-%d %H:%M:%S%z')

                evt = Event()
                evt.stanza = input_name
                evt.source = 'dynamodb://' + table_name
                evt.data = json.dumps(row, cls=DecimalEncoder)
                evt.time = evt_timestring
                ew.log('INFO', 'Input row with time ' + evt.time)
                ew.write_event(evt)

            if len(rows) > 0:
                self.update_modular_input(input_name, input_item,
                                          rows[-1][timestamp_field], ew)

if __name__ == '__main__':
    sys.exit(DynamoDBModularInput().run(sys.argv))
